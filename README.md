# Basic Unit Test Angular

Project sederhana untuk mempelajari cara kerja unit test pada front end terutama untuk angular. Tampilan simpan data kendaraan yang mana terdapat pilihan mobil dan motor yang datanya di dapat dari service. Jika kendaraan yang dipilih adalah mobil maka field baris akan muncul.

File yang ditest adalah component dan service.

## Tentang Project

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

`fit()` untuk test satuan, `xit` untuk test semua kecuali test dengan function ini.

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
