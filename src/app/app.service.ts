import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { RequestService } from './app.component';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http: HttpClient) { }

  public getTipeKendaraan():Observable<any> {
    return this.http.get('http://localhost:3000/tipe-kendaraan');
  }

  public save(param: RequestService):Observable<any> {
    return this.http.post('http://localhost:3000/save', '');
  }
}
