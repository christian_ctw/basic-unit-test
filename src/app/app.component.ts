import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { FormGroup, FormControl } from '@angular/forms';
import { TestBed } from '@angular/core/testing';

export class TipeKendaraan{
  id: number;
  typeName: string;
}

export interface RequestService{
  nama: string;
  tipe: string;
  baris: number;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'unit-test';
  tipeKendaraan: TipeKendaraan[];
  kendaraanForm: FormGroup;

  constructor(
    private service: AppService,
    ) { }

  ngOnInit(){
    this.kendaraanForm = new FormGroup({
      nama: new FormControl(null),
      tipe: new FormControl(null),
      baris: new FormControl(null)
    });
    this.service.getTipeKendaraan().subscribe((result: any)=>{
      this.tipeKendaraan = result;
    });

    // this.getKendaraan();
    
  }

  getKendaraan(){
    this.service.getTipeKendaraan().subscribe((result: any)=>{
      this.tipeKendaraan = result.type;
      console.log(this.tipeKendaraan);
    });
    
  }

  save(){
    this.service.save(this.kendaraanForm.getRawValue()).subscribe((result: any)=>{
      this.tipeKendaraan = result.type;
    });
  }
}
