import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent, TipeKendaraan } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { AppService } from './app.service';
import { of } from 'rxjs';
import { By } from '@angular/platform-browser';


describe('AppComponent', () => {

  function getResponse(){
    return [
      {
        id : 1,
        typeName : "Mobil"
      },
      {
        id : 2,
        typeName : "Motor"
      },
    ];
    
  }

  let service : jasmine.SpyObj<AppService>

  beforeEach(async(() => {
    service = jasmine.createSpyObj('AppService', ['getTipeKendaraan', 'save']);
    service.getTipeKendaraan.and.returnValue(of(getResponse()))
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        FormsModule,
        ReactiveFormsModule,
        HttpClientModule,
      ],
      declarations: [
        AppComponent
      ],
      providers: [
        {
          provide: AppService, 
          useValue: service
        }
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should show field', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.nama')).toBeTruthy();
    expect(compiled.querySelector('.tipe')).toBeTruthy();
  });

  it('should check call service', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    fixture.detectChanges();
    expect(service.getTipeKendaraan).toHaveBeenCalled();
    expect(app.tipeKendaraan).toEqual(getResponse());
    console.log(fixture.debugElement.queryAll(By.css('.option')));
    const optionHtml = fixture.debugElement.queryAll(By.css('.option')).map(element => {
      return parseInt(element.nativeElement.value);
    });
    const responseSvc = getResponse().map(resp => {
      return resp.id;
    })

    expect(optionHtml).toEqual(responseSvc);

  });

  it('should check show "baris" if tipe = mobil', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    const compiled = fixture.nativeElement;
    fixture.detectChanges();
    
    const select: HTMLSelectElement = fixture.debugElement.query(By.css('.tipe')).nativeElement;
    select.value = select.options[1].value;  // <-- select a new value
    select.dispatchEvent(new Event('change'));
    fixture.detectChanges();
    expect(compiled.querySelector('.baris')).toBeTruthy();

  });

  it('should check method save from service is called', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    const compiled = fixture.nativeElement;
    fixture.detectChanges();

    const nama: HTMLSelectElement = fixture.debugElement.query(By.css('.nama')).nativeElement;
    nama.value = "nama";
    nama.dispatchEvent(new Event('input'));

    const tipe: HTMLSelectElement = fixture.debugElement.query(By.css('.tipe')).nativeElement;
    tipe.value = tipe.options[1].value;
    tipe.dispatchEvent(new Event('change'));

    fixture.detectChanges();

    const baris: HTMLSelectElement = fixture.debugElement.query(By.css('.baris')).nativeElement;
    baris.value = '3';
    baris.dispatchEvent(new Event('input'));

    const button: HTMLSelectElement = fixture.debugElement.query(By.css('button')).nativeElement;
    button.click();

    expect(service.save).toHaveBeenCalledWith(app.kendaraanForm.getRawValue());

  });

});
