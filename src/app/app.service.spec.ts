import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { AppService } from './app.service';

describe('AppService', () => {
  let service: AppService;
  let httpTestingController: HttpTestingController;

  function getResponse(){
    return [
      {
        id : 1,
        typeName : "Mobil"
      },
      {
        id : 2,
        typeName : "Motor"
      },
    ];
    
  }

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AppService],
      imports: [HttpClientTestingModule]
    });

    httpTestingController = TestBed.get(HttpTestingController);
    service = TestBed.inject(AppService);
  });
  
  it('should get tipe kendaraan from API' , done => {
    service.getTipeKendaraan().subscribe(result => {
      expect(result).toEqual(getResponse());
      done();
    });

    const request = httpTestingController.expectOne({
      method: 'GET',
      url: `http://localhost:3000/tipe-kendaraan`
    });
    request.flush(getResponse());
  });
});
